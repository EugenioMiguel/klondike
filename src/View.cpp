#include "controllers/OperationController.h"

class  View : public OperationControllerVisitor
{
    public:
        virtual void interact(OperationController &operationController)=0;
};

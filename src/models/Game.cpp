#include "headers/Game.h"

namespace models {

	class Game{

		Game::Game() {
			state=State.INITIAL;
			board= new Board();
		}

		Game::~Game() {
			delete board;
		}

		State Game::get_state() {
			return state;
		}
	};
}

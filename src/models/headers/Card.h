/*
 * Card.h
 *
 *  Created on: 21 de abr. de 2016
 *      Author: eeugmig
 */

#ifndef SRC_MODELS_CARD_H_
#define SRC_MODELS_CARD_H_

namespace models {

class Card {
public:
	Card();
	virtual ~Card();
};

} /* namespace models */

#endif /* SRC_MODELS_CARD_H_ */

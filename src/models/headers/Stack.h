/*
 * Stack.h
 *
 *  Created on: 21 de abr. de 2016
 *      Author: eeugmig
 */

#ifndef SRC_MODELS_STACK_H_
#define SRC_MODELS_STACK_H_

namespace models {

class Stack {
public:
	Stack();
	virtual ~Stack();
};

} /* namespace models */

#endif /* SRC_MODELS_STACK_H_ */

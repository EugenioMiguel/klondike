/*
 * Suite.h
 *
 *  Created on: 21 de abr. de 2016
 *      Author: eeugmig
 */

#ifndef SRC_MODELS_SUITE_H_
#define SRC_MODELS_SUITE_H_

namespace models {

class Suite {
public:
	Suite();
	virtual ~Suite();
};

} /* namespace models */

#endif /* SRC_MODELS_SUITE_H_ */

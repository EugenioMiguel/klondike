/*
 * Color.h
 *
 *  Created on: 21 de abr. de 2016
 *      Author: eeugmig
 */

#ifndef SRC_MODELS_COLOR_H_
#define SRC_MODELS_COLOR_H_

namespace models {

class Color {
public:
	Color();
	virtual ~Color();
};

} /* namespace models */

#endif /* SRC_MODELS_COLOR_H_ */

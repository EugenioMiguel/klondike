/*
 * State.h
 *
 *  Created on: 21 de abr. de 2016
 *      Author: eeugmig
 */

#ifndef SRC_MODELS_STATE_H_
#define SRC_MODELS_STATE_H_

namespace models {

class State {
public:
	State();
	virtual ~State();
};

} /* namespace models */

#endif /* SRC_MODELS_STATE_H_ */

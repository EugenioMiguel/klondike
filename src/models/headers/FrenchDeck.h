/*
 * FrenchDeck.h
 *
 *  Created on: 21 de abr. de 2016
 *      Author: eeugmig
 */

#ifndef SRC_MODELS_FRENCHDECK_H_
#define SRC_MODELS_FRENCHDECK_H_

namespace models {

class FrenchDeck {
public:
	FrenchDeck();
	virtual ~FrenchDeck();
};

} /* namespace models */

#endif /* SRC_MODELS_FRENCHDECK_H_ */

/*
 * Pile.h
 *
 *  Created on: 21 de abr. de 2016
 *      Author: eeugmig
 */

#ifndef SRC_MODELS_PILE_H_
#define SRC_MODELS_PILE_H_

namespace models {

class Pile {
public:
	Pile();
	virtual ~Pile();
};

} /* namespace models */

#endif /* SRC_MODELS_PILE_H_ */

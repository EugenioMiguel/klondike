/*
 * Ranking.h
 *
 *  Created on: 21 de abr. de 2016
 *      Author: eeugmig
 */

#ifndef SRC_MODELS_RANKING_H_
#define SRC_MODELS_RANKING_H_

namespace models {

class Ranking {
	//arraywithlast10bestscores
public:
	Ranking();
	virtual ~Ranking();
	Score checkScoreTopTen(Score score);

};

} /* namespace models */

#endif /* SRC_MODELS_RANKING_H_ */

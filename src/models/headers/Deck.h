/*
 * Deck.h
 *
 *  Created on: 21 de abr. de 2016
 *      Author: eeugmig
 */

#ifndef SRC_MODELS_DECK_H_
#define SRC_MODELS_DECK_H_

namespace models {

class Deck {
public:
	Deck();
	virtual ~Deck();
	virtual Deck initialize();//Baraja las cartas, se le pasa el parametro elegido por el usuario
	//spanish o french

};

} /* namespace models */

#endif /* SRC_MODELS_DECK_H_ */

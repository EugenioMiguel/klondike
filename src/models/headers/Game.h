#ifndef KLONDIKE_GAME_H
#define KLONDIKE_GAME_H

namespace models {
	class Game{
		State state;
		Board board;
	public:
		Game();
		virtual ~Game();
		State get_state();
		State set_state();
	};
}

#endif //KLONDIKE_GAME_H


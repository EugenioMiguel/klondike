/*
 * Foundation.h
 *
 *  Created on: 21 de abr. de 2016
 *      Author: eeugmig
 */

#ifndef SRC_MODELS_FOUNDATION_H_
#define SRC_MODELS_FOUNDATION_H_

namespace models {

class Foundation {
public:
	Foundation();
	virtual ~Foundation();
};

} /* namespace models */

#endif /* SRC_MODELS_FOUNDATION_H_ */

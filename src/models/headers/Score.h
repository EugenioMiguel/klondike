/*
 * Score.h
 *
 *  Created on: 21 de abr. de 2016
 *      Author: eeugmig
 */

#ifndef SRC_MODELS_SCORE_H_
#define SRC_MODELS_SCORE_H_

namespace models {

class Score {
	//int score;???
public:
	Score(int numeroDeMovimientos);//calculara la puntuacion, le asignamos a esta clase la
	//responsabilidad de calcular la puntuacion, no desde fuera. Forma 1
	virtual ~Score();
	void setScore(Score score);
	Score getScore(Score score); // Forma 2-> metodo para aumentar la puntuacion
};

} /* namespace models */

#endif /* SRC_MODELS_SCORE_H_ */

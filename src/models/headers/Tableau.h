/*
 * Tableau.h
 *
 *  Created on: 21 de abr. de 2016
 *      Author: eeugmig
 */

#ifndef SRC_MODELS_TABLEAU_H_
#define SRC_MODELS_TABLEAU_H_

namespace models {

class Tableau {
public:
	Tableau();
	virtual ~Tableau();
};

} /* namespace models */

#endif /* SRC_MODELS_TABLEAU_H_ */

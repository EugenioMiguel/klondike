/*
 * SpanishDeck.h
 *
 *  Created on: 21 de abr. de 2016
 *      Author: eeugmig
 */

#ifndef SRC_MODELS_SPANISHDECK_H_
#define SRC_MODELS_SPANISHDECK_H_

namespace models {

class SpanishDeck {
public:
	SpanishDeck();
	virtual ~SpanishDeck();
};

} /* namespace models */

#endif /* SRC_MODELS_SPANISHDECK_H_ */

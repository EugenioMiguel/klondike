/*
 * Waste.h
 *
 *  Created on: 21 de abr. de 2016
 *      Author: eeugmig
 */

#ifndef SRC_MODELS_WASTE_H_
#define SRC_MODELS_WASTE_H_

namespace models {

class Waste {
public:
	Waste();
	virtual ~Waste();
};

} /* namespace models */

#endif /* SRC_MODELS_WASTE_H_ */

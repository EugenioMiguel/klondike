#ifndef KLONDIKE_IO_H
#define KLONDIKE_IO_H

#include <string>

namespace utils{

    class IO{

    public:
        IO();
        virtual ~IO();
        int read_int(std::string title);
        std::string read_string(std::string title);
        void write(std::string title);
        void write_integer(int integer);
        void writeln(std::string title);
    	void writeError(std::string formato);
    };
}

#endif

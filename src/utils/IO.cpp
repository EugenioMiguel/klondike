#include "IO.h"
#include <stdio.h>
#include <iostream>
#include <sstream>

class IO {

public:

		std::string read_string(std::string title) {
			std::string entrada;
			this->write(title);
			std::cin>>entrada;
			return entrada;
		}

		int read_int(std::string title) {
			int integer;
			this->write(title);
			scanf("%d", &integer);// falta a?adir excepciones y lo ideal seria hacer un parseint en c++ equivalente de this.readString()
			return integer;
		}

		void write_integer(int integer){
			std::cout<< integer;
		}

		void write(std::string title) {
			std::cout << title;
		}

		void writeln(std::string title) {
			std::cout << title << std::endl;
		}

		void writeError(std::string formato) {

			std::stringstream ss;
			ss << "ERROR DE FORMATO! " << "Introduzca un valor con formato " << formato << ".";
			std::string s = ss.str();
			std::cout << s;
		}
};

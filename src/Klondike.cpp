#include "Logic.h"
#include "View.cpp"
#include "views/console/ConsoleView.cpp"
#include "controllers/local/LocalLogic.cpp"

class Klondike{
    Logic logic;
    View view;
public:
    Klondike(View view, Logic logic){
        this->view = view;
        this->logic = logic;
    }

    void play(){
        OperationController controller;
        do{
            controller = logic.getOperationController();
            if (&controller != nullptr){
                view.interact(controller);
            }
        }while(&controller != nullptr);
    }

    int main() {
        Klondike klondike  = new Klondike(new ConsoleView(), new LocalLogic());
        klondike.play();
    }
};
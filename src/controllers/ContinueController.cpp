#include "OperationController.h"

class ContinueController: public OperationController
{
public:
    void resume(bool another);
};
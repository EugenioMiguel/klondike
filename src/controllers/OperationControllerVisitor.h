#ifndef KLONDIKE_OPERATIONCONTROLLERVISITOR_H
#define KLONDIKE_OPERATIONCONTROLLERVISITOR_H

#include "StartController.cpp"
#include "ColocateController.cpp"
#include "ContinueController.cpp"

class OperationControllerVisitor {
public:
    virtual void visit(StartController &startController) = 0;
    virtual void visit(ColocateController &colocateController) = 0;
    virtual void visit(ContinueController &continueController) = 0;
};

#endif


#ifndef KLONDIKE_LOGIC_H
#define KLONDIKE_LOGIC_H
#include "controllers/OperationController.h"

class Logic
{
public:
    OperationController getOperationController();
};

#endif

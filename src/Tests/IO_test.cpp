#include "src/utils/IO.cpp"
using namespace std;

int main() {
	IO *entradaSalida= new IO();
	int entero=0;
	entradaSalida->writeln("Test unitario de la clase IO:");
	entero=entradaSalida->read_int("Introduzca un entero: ");
	entradaSalida->writeln("el entero introducido es: ");
	entradaSalida->write_integer(entero);
	string entradacadena="";
	entradacadena=entradaSalida->read_string("Introduzca un string: ");
	entradaSalida->writeln("El string introducido es: ");
	entradaSalida->writeln(entradacadena);
	return 0;
}


class StartView {

    BoardView boardView;
    public:
    StartView(BoardView boardView) {
        this->boardView = boardView;
    }

    void interact(StartController startController){
        //LimitedIntDialog.instance().read("Cu�ntos usuarios?", 0, 2);
        int users = 1;
        startController.start(users);
        boardView.write(startController);
    }
};

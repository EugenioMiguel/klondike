#include "../../View.cpp"

class ConsoleView: public View
{
    StartView startView;
    GameView gameView;
    ContinueView continueView;

    public:
        ConsoleView(){
                BoardView boardView = new BoardView();
                startView = new StartView(boardView);
                gameView = new GameView(boardView);
                continueView = new ContinueView();
        }

    void interact(OperationController operationController) {
            operationController.accept(*this);
    }

    void visit(StartController startController) {
            startView.interact(startController);
    }

    void visit(ColocateController colocateController) {
            gameView.interact(colocateController);
    }

    void visit(ContinueController continueController) {
            continueView.interact(continueController);
    }
};